import java.util.*;
import java.io.*;

public class whereami {
    public static void main(String args[]) throws Exception {
        File in = new File("whereami.in");
        File out = new File("whereami.out");
        
        Scanner sc = new Scanner(in);
        PrintWriter pw = new PrintWriter(out);
        
        int N = sc.nextInt();
        String S = sc.next();
        
        for (int K = 1; K <= N; ++K) {
            boolean all_unique = true;
            for (int i = 0; i <= N-K; ++i) {
                for (int j = i+1; j <= N-K; ++j) {
                    boolean same = true;
                    for (int k = 0; k < K; ++k) {
                        if (S.charAt(i+k) != S.charAt(j+k)) same = false;
                    }
                    if (same) all_unique = false;
                }
            }
            if (all_unique) {
                pw.println(K);
                break;
            }
        }
        
        sc.close();
        pw.close();        
    }
}
