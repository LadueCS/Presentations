# Libre Software

## What's libre software?

### What do you think these three terms mean?

* Libre software
* Free software
* Open-source software

They actually all mean the same thing!

### What are some examples of libre software?

* VLC media player
* GCC compiler
* Firefox web browser
* GNU+Linux operating system
* Blender 3D modeler
* Apache and nginx web servers
* SuperTuxKart racing game
* Wikipedia
* WordPress
* Python
* Vim editor
* Git version control system

### What are some advantages of libre software?

* You can look at the source code yourself to see exactly what the software is doing.
* Even if you aren't good at programming, other developers will be able to contribute improvements to open-source projects.
* The communities around open-source projects are much more open, welcoming, and involved in the development of the project.
* It's entirely free.
* Libre software tends to be more secure due to more people looking at the code and the community's involvement in development.
* No one is forcing you to do anything because you're in control.

## History of libre software

* 1980: Richart Stallman gets triggered about a printer at MIT that breaks down constantly and they don't have the source code to modify the printer's software.
* 1983: Stallman creates the GNU project to develop an entirely free operating system and founds the Free Software Foundation.
* 1991: GNU is almost complete but missing a kernel. A Finnish college student named Linus Torvalds writes a kernel in his spare time and releases it online. People combine the GNU software with Linux to create what we now call "Linux"